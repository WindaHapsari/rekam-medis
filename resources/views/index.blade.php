@extends('layout/main')

@section('title', 'Rekam Medis Medika')

@section('container')
	<div class="container">
    <div class="row">
      <div class="col-9">
        <h1 class="mt-4">Rekam Medis Medika</h1> <br>
      </div>
    </div>
  </div>

<div class="container">
    <div class="row">
      <div class="col-9">
        <div class="card border-primary mb-3" style="max-width: 80rem;">
          <div class="card-header">Rekam Medis Medika</div>
          <div class="card-body text-primary">
            <h5 class="card-title">Salam Sehat Medika</h5>
            <p class="card-text">
              Emisi Otoakustik (OAE) telah digunakan secara luas untuk membantu pendengaran bayi yang baru lahir, termasuk di Rekam Medis Medika. Layanan ini secara klinis sangat penting sebagai dasar pengujian sederhana dan non-invasif terhadap kelainan pendengaran pada bayi baru lahir dan anak-anak yang belum dapat menerima tes pendengaran secara konvensional. OAE adalah produk unggulan di rumah sakit ini sejak pendiriannya pada tahun 2006, dengan dukungan fasilitas KTK.
              
              <br><br>
              
              Fasilitas
              <ul>
                <li>KTK (Klinik Tumbuh Kembang)</li>
                <li>Gigi Spesialistik</li>
                <li>PICU (Unit Perawatan Intensif Anak)</li>
                <li>EEG (Elektroensefalografi)</li>
                <li>Uroflometri</li>
                <li>Radiografi Gigi</li>
                <li>Radiografi Panoramik Gigi</li>
                <li>ICU/NICU (Intensive Care Unit/Neonatal Intensive Care Unit)</li>
                <li>Perina</li>
                <li>Gymnasium</li>
                <li>Spirometri</li>
                <li>Audiometri</li>
              </ul>
              
              Jam Kunjungan
              <ul>
                <li>11.00 – 12.00 WIB</li>
                <li>17.00 – 19.00 WIB</li>
              </ul>
            </p>
          </div>
        </div>
    </div>
  </div>
</div>



@endsection