@extends('layout/main')

@section('title', 'Dokter')

@section('container')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="mt-4">Data Dokter</h1>

				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">No</th>
							<th scope="col">Nama</th>
							<th scope="col">NPA</th>
							<th scope="col">Alamat Praktek</th>
							<th scope="col">Nomor Telp</th>
							<th scope="col">Spesialisasi</th>
							<th scope="col">Aksi</th>
						</tr>
					</thead>

					<tbody>
						@foreach( $dokter as $psn)
							<tr>
								<th scope="row">{{ $loop->iteration }}</th>
								<td>{{ $psn->nama }}</td>
								<td>{{ $psn->npa }}</td>
								<td>{{ $psn->alamat_praktek }}</td>
								<td>{{ $psn->nomor_telp }}</td>
								<td>{{ $psn->spesialisasi }}</td>
								<td>
									<a href="" class="badge bg-success">Edit</a>
									<a href="" class="badge bg-danger">Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div>
@endsection