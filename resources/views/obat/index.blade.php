@extends('layout/main')

@section('title', 'Obat')

@section('container')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="mt-4">Data Obat</h1>

				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">No</th>
							<th scope="col">Nama Obat</th>
							<th scope="col">Nomor Obat</th>
							<th scope="col">Kategori</th>
							<th scope="col">Stok</th>
							<th scope="col">Produksi</th>
							<th scope="col">Aksi</th>
						</tr>
					</thead>

					<tbody>
						@foreach( $obat as $psn)
							<tr>
								<th scope="row">{{ $loop->iteration }}</th>
								<td>{{ $psn->nama_obat }}</td>
								<td>{{ $psn->nomor_obat }}</td>
								<td>{{ $psn->kategori }}</td>
								<td>{{ $psn->stok }}</td>
								<td>{{ $psn->produksi }}</td>
								<td>
									<a href="" class="badge bg-success">Edit</a>
									<a href="" class="badge bg-danger">Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div>
@endsection