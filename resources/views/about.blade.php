@extends('layout/main')

@section('title', 'About')

@section('container')
	<div class="container">
		<div class="row">
			<div class="col-9">
				<h1 class="mt-4">About Us</h1><br>

				<div class="card text-white bg-primary mb-3" style="max-width: 80rem;">
				  <div class="card-header">Rekam Medis Medika</div>
				  <div class="card-body">
				    <h5 class="card-title">Hello, {{$note}} guys</h5>
				    <p class="card-text">
				    	Selamat datang di Rekam Medis Medika. Rekam Medis Medika memiliki dua cabang yakni Medika Utama Ciliwung dan Medika Utama Pakuwon City. Rekam Medis Medika Ciliwung didirikan pada tanggal 31 Juli 2005 sedangkan Rekam Medis Medika Pakuwon City pada tanggal 10 Agustus 2009.

				    	<br><br>
						
						Kini Rekam Medis Medika berkolaborasi dengan Klinink Sanodoc di Jln Ciliwung no 54 Surabaya dalam hal pemeriksaan kesehatan, pengadaan seminar awam untuk pasien dan promo obat-obatan khusus untuk pasien.
						
						<br><br>
						
						Rekam Medis Medika dan Klinik Sanodoc menyediakan pelayanan klinik dokter umum sampai dengan dokter spesialis dengan peralatan lengkap yang buka mulai jam 07.00 – 22.00 WIB. Dengan website ini kami ingin memperkenalkan berbagai pelayanan yang tersedia di Rekam Medis Medika dan Klinik Sanodoc. Selain itu kami juga ingin memberikan informasi yang berharga mengenai kesehatan dan ilmu kedokteran bagi semua orang yang mendambakan hidup sehat.
					</p>
				  </div>
				</div>
			</div>
		</div>
    </div>
@endsection