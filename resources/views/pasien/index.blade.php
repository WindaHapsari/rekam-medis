@extends('layout/main')

@section('title', 'Pasien')

@section('container')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="mt-4">Data Pasien</h1>

				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">No</th>
							<th scope="col">Nama</th>
							<th scope="col">Nomor Pasien</th>
							<th scope="col">Alamat</th>
							<th scope="col">Nomor Telp</th>
							<th scope="col">Riwayat Sakit</th>
							<th scope="col">Aksi</th>
						</tr>
					</thead>

					<tbody>
						@foreach( $pasien as $psn)
							<tr>
								<th scope="row">{{ $loop->iteration }}</th>
								<td>{{ $psn->nama }}</td>
								<td>{{ $psn->nomor_pasien }}</td>
								<td>{{ $psn->alamat }}</td>
								<td>{{ $psn->nomor_telp }}</td>
								<td>{{ $psn->riwayat_sakit }}</td>
								<td>
									<a href="" class="badge bg-success">Edit</a>
									<a href="" class="badge bg-danger">Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div>
@endsection